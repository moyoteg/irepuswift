# README #

### What is this repository for? ###

* This repo is meant as a code showing.
* Version 0.1

### How do I get set up? ###

* This project uses Cocopods so running "pod install" on the terminal might be a good idea. =)
* Open workspace.

### Who do I talk to? ###

* Repo owner or admin @moyoteg (http://moigutierrez.me)

### Extras
* This is my POSTMAN collection if you would like to see how I tested the endpoints for http://whoismyrepresentative.com:

https://www.getpostman.com/collections/047656887b1cb82bea8a

* POSTMAN environment:


```
#!
{
	"id": "77649fde-b7d8-8062-0e46-83cf527bb3e6",
	"name": "Who is my representative",
	"values": [
		{
			"key": "host",
			"value": "http://whoismyrepresentative.com/",
			"type": "text",
			"name": "host",
			"enabled": true
		}
	],
	"timestamp": 1458666493600,
	"synced": false,
	"syncedFilename": "",
	"team": null,
	"isDeleted": false
}


```

# API #
[Who represents You in the U.S. Congress](http://whoismyrepresentative.com)

### Please specify what features you implemented

* Get all Representative by zip code.
* Get all Senators by name/state.
* Get all Representatives by name/state.
* Call rep directly from Profile view.
* Open rep website directly from Profile view.
* Visualize rep's office within the Profile view.

### Deficiencies remain in the app ###

* No unit tests.
* No good looking assets for the app icon.
* No name sanitation before search.
* No local caching.

### Include a short paragraph on what you did to make the app faster/more stable and which feature(s) you are most proud of 

I am proud that the app is pretty simple and it is really easy to use, gives user prompt feedback and allows the user to quickly look at all the results.

* Used async networking to avoid locking the main thread.
* Used well maintained frameworks such as UIKit and Alamofire.


To do:

- [ ] useful app
- [ ] written in Swift
- [ ]  out of this public api: http://whoismyrepresentative.com/api.
- [ ] goal here is not to make the most flashy or attractive app.
- [ ] We're looking for a solid, 
- [ ] stable application that provides real utility.
- [ ] Impress us with how well you have thought about the use cases your users will encounter. 
- [ ] as they use your app on an iPhone and iPad (iOS 7 and later)
- [ ] while operating on unreliable networks.
- [ ] We will be looking for effective use of platform APIs
- [ ] as well as clean
- [ ] and well-documented code.
- [ ] It's up to you to decide on things like features.
- [ ]  You have 72 hours from the time you start to when you should submit your solution.
- [ ]  It doesn't have to be feature complete,
- [ ]  but it should compile and run. 
- [ ] Your submission should include full, buildable source code. 
- [ ] Please specify what features you implemented 
- [ ] and what known issues (deficiencies) remain in the app.
- [ ] Include a short paragraph on what you did to make the app faster/more stable and which feature(s) you are most proud of.